import os
import time
import distutils.dir_util

class UsbHandler:
    # /param thresh The threshold number of bytes before changing USB sticks.
    # The default value is 5e6 or 5 MB.
    # /param device The name of the device to write to
    # /param filename The name of the file to write to. It does not have to exist.
    # /param media The folder that contains the mounted filesystem. In the
    # case of raspberry pi, it should be /media/pi
    def __init__(self,
                 thresh=12e6,
                 device="",
                 directory="dir.txt",
                 filename="data.txt",
                 media="/media/pi"
                 ):
        """Creates a new UsbHandler object. If a USB stick becomes filled with data
        then the UsbHandler will switch USB sticks to continue filling with data"""

        self.usb_root_path = media
        # the current USB name
        self.curr = device

        # the amount of space remaining on the current USB
        self.remaining = 0

        # the amount of buffer space before a USB completely fills
        self.threshold = thresh

        # the name of the filename the pi writes to
        self.filename = filename
        # the name of the directory the pi writes to
        self.curr_dir = directory

        # self.filepath = '/'+ self.curr +'/'+ self.curr_dir + '/' + self.filename
        # self.filepath = '/'+ self.curr +'/'+ self.filename

        # self.makedir(self.filepath)

        dirs = os.listdir(self.usb_root_path)
        if len(dirs) == 0:
            print "There is no removable media device"
        if self.curr == "":
            self.curr = self.findAvailableUSB()
            print "Starting to write on device " + self.curr + '/' + self.curr_dir + " for file " + filename
            self.remaining = self.getAvailableSpaceForCurrentUSB()
        else:
            print "Starting to write on device " + self.curr + '/' + self.curr_dir + " for file " + filename
            self.remaining = self.getAvailableSpaceForCurrentUSB()

        #print self.usb_root_path + '/' + self.curr
        #print os.path.ismount(self.usb_root_path + '/' + self.curr)

        count = 0

        #wait until the drive is mounted
        #if not mounted after 30 seconds, just use file system
        while(not os.path.ismount(self.usb_root_path + '/' + self.curr)):
            # print "waiting for usb to mount  "
            # print count
            time.sleep(0.5)
            count += 1
            if count >= 5:
                self.curr = self.findAvailableUSB()
                self.remaining = self.getAvailableSpaceForCurrentUSB()
                count = 0
                break 
        

         # the io stream
        self.filepath = '/'+ self.curr +'/'+ self.curr_dir + '/' + self.filename
        self.makedir(self.usb_root_path + '/'+ self.curr +'/'+ self.curr_dir + '/')
        self.file = open(self.usb_root_path + self.filepath , 'a+')
             
    def makedir(self, path):
        if not os.path.exists(path):
            os.makedirs(path)
        
    # Finds a USB with space on it
    def findAvailableUSB(self):
        dirs = os.listdir(self.usb_root_path)
        for name in dirs:
            path = self.usb_root_path + '/' + name
            statvfs = os.statvfs(path)
            free_blocks = statvfs.f_frsize * statvfs.f_bavail
            if free_blocks > self.threshold:
                return name
        print "No available USB ports left"
        return '.'

    # Finds the remaining space in the current USB
    def getAvailableSpaceForCurrentUSB(self):
        path = self.usb_root_path + '/' + self.curr
        if os.path.isdir(path):
            statvfs = os.statvfs(path)
            return statvfs.f_frsize * statvfs.f_bavail
        else:
            return -1

    # Write content to the current USB
    def write(self, content):
        if self.remaining < self.threshold:
            self.curr = self.findAvailableUSB()
            self.remaining = self.getAvailableSpaceForCurrentUSB()

            # if there is another USB to switch to, do it
            if self.remaining > 0:
                self.file.close()
                print self.usb_root_path
                while(os.path.ismount(self.usb_root_path + '/' + self.curr)):
                    time.sleep(0.5)

                count = 0
                while(not os.path.ismount(self.usb_root_path + '/' + self.curr)):
                    print "waiting for usb to mount"
                    time.sleep(0.5)
                    count += 1
                    if count >= 60:
                        self.curr = self.findAvailableUSB()
                        self.remaining = self.getAvailableSpaceForCurrentUSB()
                        count = 0
                self.file = open(self.usb_root_path + self.filepath, 'a+')
                # self.file = open(self.usb_root_path + '/' + self.curr +
                #                '/' + self.curr_dir + '/' + self.filename, 'a+')
                print "Switching to " + self.curr + " USB"
        if self.curr == '.':
            return

        self.remaining = self.getAvailableSpaceForCurrentUSB()
        self.file.write(content)

# MARK-- ACCESSORS

    # Gets the device that is currently being written to
    def getDevice(self):
        return self.curr

    # Gets the name of the file that the library is writing to
    def getFilename(self):
        return self.filename

    # Gets the name of the file that the library is writing to
    def getCurrDir(self):
        return self.curr_dir

    # Returns a list of full USB sticks
    def getFull(self):
        full = []
        dirs = os.listdir(self.usb_root_path)
        for name in dirs:
            path = self.usb_root_path + '/' + name
            statvfs = os.statvfs(path)
            free_blocks = statvfs.f_frsize * statvfs.f_bavail
            if free_blocks > self.threshold:
                continue
            else:
                full.append(name)
        return full

    # If there is no need to write to USB anymore, close the channel
    def close(self):
        self.file.close()
