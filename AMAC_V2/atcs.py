import os
import PsocHandler
import time
import RPi.GPIO as GPIO
   
# FIXME JPR:
# 1) Look at PsocHandler
# 2) Look at AM.PsocAnIO()
   
count = 0

AM = None # PsocHandler.PsocHandler(filename="data.txt" )

def openAM(directory, filename):
    global AM
    print "creating new PsocHandler with Testname = '%s'"%(filename)
    AM = PsocHandler.PsocHandler(directory = directory,filename = filename)

# def AmacTest(testName,range,reps):
#     openAM(testName)
#     AM.PsocAmTest('#A',range,reps)
  
# def AmacPreradTest(curr_dir,testName,range,reps):
    
#     openAM(curr_dir, testName)
#     AM.resetPsoc()
#     time.sleep(5)
#     AM.PsocAmTest('#P',range,reps)
    # AM.PsocAnIO('#G', AdcValue, DacValue)
    # AM.PsocAmTest('#E',range,reps)
    # AM.PsocAmTest('#A',range,reps)
    #1 Create new folder each day
    #2 Create new data file for each group of tests  
    #3 Safe each daily group test results in new created folder from #1
    #4 At the end of the day archive created folder with test  

def AmacANIO_Cal(curr_dir,testName,adcValue,dacValue,reg,regData):
    openAM(curr_dir, testName)
    AM.resetPsoc()
    time.sleep(4)
    # FIXME check on these
    AM.PsocAnIO('#P', 200, 10, 10, 10)
    # time.sleep(2)
    # AM.PsocAnIO('#C', 160, 2, 40, 10 )
    time.sleep(2)
    AM.PsocAnIO('#D', 163, 1, 45, 68)
    time.sleep(2)
    AM.PsocAnIO('#D', 160, 1, 45, 85)
    time.sleep(2)
    AM.PsocAnIO('#D', 160, 1, 45, 102)
    time.sleep(2)
    AM.PsocAnIO('#D', 160, 1, 45, 119)
    
    
if __name__ == "__main__":
    count =+ 6 # FIXME is this to make it a unique output?
    testName = ""
    test_seq = str(count)
    testName = "Am_preradTest0" + test_seq
    # AmacPreradTest("AnIOTest"+"08112017",testName,184,500)
    # FIXME want to update this daily? could just use time module, i think
    AmacANIO_Cal("AnIOTest"+"08242017",testName,160,1,1,1)

# AM.PsocReadPRD('#P') 
