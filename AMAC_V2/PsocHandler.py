import os
# import smbus
import time
import serial as pyserial
import RPi.GPIO as GPIO
import SerialHandler
import UsbHandler
from datetime import datetime

class PsocHandler:
    # /param AmTest = Current or next AMAC TEST 
    # /param filename The name of the file to write to. It does not have to exist.
  
    def __init__(self,
                 AmTest="",
                 directory="dir.txt",
                 filename="data.txt"
                 ):

        # the next test to execute on AM
        self.AMAC_TEST = AmTest
        # the name of the filename the pi writes to
        self.filename = filename
        # the name of the directory the pi writes to
        self.dir = directory
       
        self.usb = UsbHandler.UsbHandler(directory = self.dir, filename = self.filename )

        """Creates a new PsocHandler object."""
        self.com = pyserial.Serial(
            port="/dev/ttyS0",
            baudrate=115200,
            parity=pyserial.PARITY_NONE,
            stopbits=pyserial.STOPBITS_ONE,
            bytesize=pyserial.EIGHTBITS,
            timeout=1, xonxoff=0, rtscts=0
        )
        # GPIO.setmode(GPIO.BCM)
        # GPIO.setwarnings(False)
        # GPIO.setup(17,GPIO.OUT)
        # GPIO.output(17,GPIO.HIGH)
        # time.sleep(1)
        # GPIO.output(17,GPIO.LOW)
        # time.sleep(1)
        # GPIO.output(17,GPIO.HIGH)
        self.xres = GPIO
        self.xres.setmode(GPIO.BCM)
        self.xres.setwarnings(False)
        self.xres.setup(22,GPIO.OUT)
        self.xres.output(22,GPIO.HIGH)
        
        
     
        
        
        
        

    def PsocWrite(self, data):
        buff_size = 16
        cnter = 0
        message = data.encode('utf-8')
        messeagelen = len(message)
        batches = len(message) / buff_size
        if batches < 1:
            batches = 1
        for i in range(0, batches):
            self.com.write(message[buff_size * i:])
            time.sleep(0.5)
            print(messeagelen)
            if batches == 1:
                break

    def PsocAmTest(self, test, range, repeat, reg, regdata):
            message_i = ''
            timeout = 6
            period = 0000
            Freq = 0.000
            Address = 14
            Register= 50
            RegData = range
            ReadCount = repeat
            
            Address = self.toHex(Address)
            Register= self.toHex(Register)
            RegData = self.toHex(RegData)
            ReadCount = self.toHex(ReadCount)

            self.com.flushInput()  #flush input buffer, discarding all its contents
            self.com.flushOutput() #flush output buffer, aborting current output 
            if test == '#A':
                padding ='RUN_All_TEST.\n'
                message_o = test + Address + Register + RegData + ReadCount + padding 
            if test == '#B':
                padding ='AMAC_RegTEST.\n'
                message_o = test + Address + Register + RegData + ReadCount + padding
            if test == '#C':
                padding ='I2C_AdrScan\n'
                message_o = test + Address + Register + RegData + ReadCount + padding
            if test == '#D':
                padding ='AMAC_RampTst.\n'
                message_o = test + Address + Register + RegData + ReadCount + padding
            if test == '#E':
                padding ='AMAC  PWR_UP.\n'
                message_o = test + Address + Register + RegData + ReadCount + padding
            if test == '#F':
                padding ='AMAC_AdcScan.\n'
                message_o = test + Address + Register + RegData + ReadCount + padding
            if test == '#G':
                padding ='ADC  DAC_Cal.\n'
                message_o = test + Address + Register + RegData + ReadCount + padding    
            elif test == '#P':
                padding ='AMAC_ReadPRD.\n'
                message_o = test + Address + Register + RegData + ReadCount + padding
            self.PsocWrite(message_o)
            print("\nMessage Sent= %s"%message_o)
            self.ReadData('AmRegTest')

    def PsocAnIO(self, test, AdcValue, DacValue,Register,RegData):
            message_i = ''
            timeout = 6
            DacCnt = 0
            Address = 14
            Reps = 0
            # register = 40
            # RegData = 3
            DacCnt = (65535 * DacValue)/2500

            print("\nDac Count= %s"%DacCnt)
            Address = self.toHex(Address)
            DacCnt = self.toHex16(DacCnt)
            AdcValue = self.toHex(AdcValue)
            Reps = self.toHex16(DacValue)
            Register = self.toHex(Register)
            RegData = self.toHex(RegData)
            print("\nDac Count= %s"%DacCnt)

            self.com.flushInput()  #flush input buffer, discarding all its contents
            self.com.flushOutput() #flush output buffer, aborting current output 
            if test == '#B':
                padding ='AMregTst.\n'
                message_o = test + Address + AdcValue + DacCnt + Register + RegData + padding

            if test == '#C':
                padding ='AM_DC_SN.\n'
                message_o = test + Address + AdcValue + Reps + Register + RegData + padding
                
            if test == '#D':
                padding ='AM Ramp .\n'
                message_o = test + Address + AdcValue + DacCnt + Register + RegData + padding
            
            if test == '#E':
                padding ='AM_DC_SN.\n'
                message_o = test + Address + AdcValue + DacCnt + Register + RegData + padding
            
            if test == '#G':
                padding ='ANIO_Cal.\n'
                message_o = test + Address + AdcValue + DacCnt + Register + RegData + padding

            elif test == '#P':
                padding ='AM_RdPRD.\n'
                message_o = test + Address + AdcValue + Reps + Register + RegData + padding

            self.PsocWrite(message_o)
            print("\nMessage Sent= %s"%message_o)
            self.ReadData('AmRegTest')

    def toHex(self, dec):
        hexvalue = '0'
        value = hex(int(dec)).split('x')[1]
        # print("\nToHexValue = %s"%value)
        if len(value) == 1:
            hexvalue = '0' + value
            # return  hexvalue.upper()
        elif len(value) == 2:
            hexvalue = value
            # return  hexvalue.upper()
        elif len(value) == 3:
            hexvalue = '0' + value
            # print("Value=%s"%hexvalue.upper())
            # return hexvalue.upper()
        # else:
        # print("Value=%s"%hexvalue.upper())
        return hexvalue.upper()

    def toHex16(self, dec):
        hexvalue = '0'
        value = hex(int(dec)).split('x')[1]
        # print("\nToHexValue = %s"%value)
        if len(value) == 1:
            hexvalue = '000' + value
            # return  hexvalue.upper()
        elif len(value) == 2:
            hexvalue = '00' + value
            # return  hexvalue.upper()
        elif len(value) == 3:
            hexvalue = '0' + value

        else:
            hexvalue = value
            # print("Value=%s"%hexvalue.upper())
            # return hexvalue.upper()
        # else:
        # print("Value=%s"%hexvalue.upper())
        return hexvalue.upper()

    def AppendToFile(self, directory, filename,  content):
        with open(directory + '/' + filename, 'a+') as f:
            f.write(content)


    def WriteToUSB(self,content):
        dirs = os.listdir(usb_path)
        for name in dirs:
            path = usb_path + '/'+ self.dir +'/' + name
            statvfs = os.statvfs(path)
            free_blocks = statvfs.f_frsize * statvfs.f_bavail
            if free_blocks > (usb_mb_threshold * 1e6): # FIXME where is usb_mb_threshold defined? maybe just a missing file
                self.AppendToFile(usb_filename, content)
            else:
                continue


    def PsocDate(self):
        message_o = " "
        cmd = '#D'
        padding = '00\n'

        # FIXME could make a getTimeStr() function
        message_o = cmd + \
            str(datetime.now().strftime("%Y-%m-%d %H:%M:%S")) + padding
        self.PsocWrite(message_o)
        print(message_o)


    def ReadData(self, filename):
        message_i = " "
        TestData = " "
        space =","
        count = 0
        print("\nMessage Recvd= %s"%message_i)
        while '@'not in message_i: 
                '''wait for command echo '''
                message_i = self.PsocRead('\n')
                print("%s"%message_i) #FIXME probably don't need the %s here? maybe need to str() it though
                count += 1
                if '\n' in message_i:
                    break
                # if count == 5000:
                #     #self.reetPsoc()
                #     break
        message_i = " "
        while '#'not in message_i: 
            message_i = self.PsocRead('\n')
            if '#' in message_i:
                    break
            else :
                    # print "writing"
                    TestData = str(datetime.now().strftime("%Y-%m-%d %H:%M:%S")) + space + message_i
                    self.usb.write(TestData)
            print("%s" % TestData)
            


    def PsocRead(self,string):
        msg = ""
        mycmd = ""
        try:
            while(mycmd != string):
                msg += mycmd
                mycmd = self.com.read()
        except Exception:
            # print "Serial Error"
            pass
        return msg

    def resetPsoc(self):
        self.xres.output(22,GPIO.HIGH)
        time.sleep(1)
        self.xres.output(22,GPIO.LOW)
        time.sleep(3)
        self.xres.output(22,GPIO.HIGH)
# MARK-- ACCESSORS

    # Gets the name of the file that the library is writing to
    def getFilename(self):
        return self.filename
