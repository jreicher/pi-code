import os
# import smbus
import time
import serial as pyserial
import SerialHandler
import UsbHandler
from datetime import datetime

sh = SerialHandler
serial = pyserial.Serial(
	port="/dev/ttyS0",\
    baudrate=115200,\
    parity=pyserial.PARITY_NONE,\
    stopbits=pyserial.STOPBITS_ONE,\
    bytesize=pyserial.EIGHTBITS,\
	timeout=1,xonxoff=0,rtscts=0
	)
usb = UsbHandler.UsbHandler(filename="test.txt")

def PsocWrite(data):
	buff_size=16
	cnter = 0
	message = data.encode('utf-8')
	batches = len(message)/buff_size
	if batches < 1:
		batches = 1
	for i in range(0,batches):
		serial.write(message[buff_size*i:])
		time.sleep(0.010)
		# print("%s" % (message[buff_size*i:]))
		if batches == 1:
			break

def PsocReadPRD(string):
	message_i = '#'
	period = 0000
	timeout = 500
	Freq = 0.000
	# Vlow = (float(self.Address.GetValue()))*1000
	# Vhigh = (float(self.Register.GetValue()))*1000
	serial.flushInput()  #flush input buffer, discarding all its contents
	serial.flushOutput() #flush output buffer, aborting current output     
	# message_o = string + self.toHex(Vlow)+self.toHex(Vhigh)+self.toHex(self.ReadCount.GetValue())+'@'
	message_o ='#P0123456789ABC@'
	PsocWrite(message_o)
	message_i = PsocRead('\n')
	print("\nMessage Sent= %s"%message_o)
	print("\nMessage Recvd= %s"%message_i)
	# print("\nPsoc Period measurement is Running \n")
	message_i = PsocRead('\n')
	
	
	"""wait until Freq measurement process is complete"""
	'''********************************************************************* '''
		
def PsocRGB(string):
	# self.isRunning_PsocReadADC_RGB = True
	print("\nPsocRGB is Running.")
	message_i = "#"
	Volts = 0000
	timeout =6
	serial.flushInput()  #flush input buffer, discarding all its contents
	serial.flushOutput() #flush output buffer, aborting current output
	Vlow = 1
	Vhigh = 2
	# message_o = string + toHex(Vlow)+toHex(Vhigh)+toHex(Vhigh)+'@'
	message_o ='#R0123456789ABC@'
	PsocWrite(message_o)
	print("\nMessage Sent= %s"%message_o)

	# while '@'not in message_i: # or self.progmode != 1
	# 	'''wait for command echo '''
		
	# 	print("\nMessage Recvd= %s"%message_i)
	# 	timeout -= 1
	# 	print("\nTime= %s"%timeout)
	# 	if timeout <= 1:
    # 			break
	message_i = PsocRead('\n')
	print("\nMessage Recvd= %s"%message_i)
	print("\nPsocRGB Done!!.")
	timeout = 6
	while '\n'not in message_i:	
		"""monitor Robots sensor status until measureZ process is complete"""
		# wx.Yield()
		message_i = PsocRead('\n')
		print("%s \n"%message_i)
		timeout -= 1
		print("\nTime= %s"%timeout)
		if timeout <= 1:
			break

def toHex(dec):
	hexvalue = '0'
	value = hex(int(dec)).split('x')[1]
	# print("\nToHexValue = %s"%value)
	if len(value) == 1:
		hexvalue = '000'+ value
		# return  hexvalue.upper()
	elif len(value) == 2:
		hexvalue = '00'+value
		# return  hexvalue.upper()
	elif len(value) == 3:
		hexvalue = '0'+ value
		# print("Value=%s"%hexvalue.upper())
		# return hexvalue.upper()
	# else:
	# print("Value=%s"%hexvalue.upper())
	return hexvalue.upper()

def AppendToFile(filename, content):
        with open(filename, 'a+') as f:
                f.write(content)

def WriteToUSB(content):
        dirs = os.listdir(usb_path)
        for name in dirs:
                path = usb_path + '/' + name
                statvfs = os.statvfs(path)
                free_blocks = statvfs.f_frsize * statvfs.f_bavail
                if free_blocks > (usb_mb_threshold * 1e6) :
                        AppendToFile(usb_filename, content)
                else :
                        continue
                                     
        

def PsocDate():
        PsocWrite(str(datetime.now()))
	
def ReadData():
	while True:
		message_i = PsocRead('\n')
		if message_i == 'GET ME THE DATE':
                        PsocDate()
                else :
                        usb.write(message_i)
		print("%s"%message_i)
			
def PsocRead(string):
        msg = ""
        mycmd = ""
        try:
            while(mycmd != string): 
                msg += mycmd
                mycmd=serial.read()
        except Exception:
            print "Serial Error"  
        return msg    	
	
# PsocRGB('#R')
# time.sleep(0.2)
# PsocReadPRD('#B')

ReadData()
# serial.close() 


	
